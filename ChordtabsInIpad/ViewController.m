//
//  ViewController.m
//  ChordtabsInIpad
//
//  Created by Peerasak Unsakon on 1/11/13.
//  Copyright (c) 2013 FireOneOne. All rights reserved.
//

#import "ViewController.h"
#import "Connection.h"
#import "NSString+URLEncoding.h"

@interface ViewController () <ConnectionDelegate, UIWebViewDelegate>
{
    __weak IBOutlet UITextField *_searchField;
    __weak IBOutlet UIButton *_searchButton;
    __weak IBOutlet UIImageView *_chordImage;
    __weak IBOutlet UILabel *_imgUrlLabel;
    __weak IBOutlet UIWebView *_webview;
    
    Connection *_connection;
    Connection *_connectionInstance;
    
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    if (_connection == nil) {
        _connection = [[Connection alloc]init];
        _connection.delegate = self;
    }
    
    if (_connectionInstance == nil) {
        _connectionInstance = [Connection getInstance];
    }
    
    _webview.delegate = self;
    _webview.hidden = YES;
    _chordImage.hidden = YES;    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searchButtonTapped:(id)sender
{
    if ([_searchField.text length] > 0) {
        [self googleSearch:_searchField.text];
        _chordImage.hidden = YES;
        _webview.hidden = NO;
    }
}

- (void)musicSearchSuccessed:(NSURL *)chordUrl
{
    [_chordImage setImageWithURL:chordUrl];
    _imgUrlLabel.text = [NSString stringWithFormat:@"%@",chordUrl];
}

- (void)googleSearch:(NSString *)songName
{
    songName = [songName urlEncodeUsingEncoding:NSUTF8StringEncoding];
    

    
//    NSURL *_url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.google.co.th/webhp?#hl=en&q=%@+site:chordtabs.in.th&fp=1&bav=on.2,or.r_gc.r_pw.r_cp.r_qf.&cad=b",songName]];

    NSURL *_url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.google.com/cse?cx=011882714379325906638:sls6ll7i5fu&ie=UTF-8&q=%@&sa=%@&siteurl=www.google.com/cse/home?cx=011882714379325906638:sls6ll7i5fu&ref=&ss=2440j1471680j9#gsc.tab=0&gsc.q=%@&gsc.page=1",songName, [@"ค้นหา" urlEncodeUsingEncoding:NSUTF8StringEncoding], songName]];
    
    NSURLRequest *_requestObj = [NSURLRequest requestWithURL:_url];
    [_webview loadRequest:_requestObj];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *currentURL = _webview.request.URL.absoluteString;
    
    [_connection getImageURLFromLink:currentURL];

}

- (void)getImageUrlSucceesed:(NSURL *)chordUrl
{
    _webview.hidden = YES;
    _chordImage.hidden = NO;    
    [_chordImage setImageWithURL:chordUrl];
    _imgUrlLabel.text = [NSString stringWithFormat:@"%@",chordUrl];

}

@end
