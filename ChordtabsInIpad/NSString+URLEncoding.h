//
//  NSString+URLEncoding.h
//  ChordtabsInIpad
//
//  Created by Peerasak Unsakon on 1/11/13.
//  Copyright (c) 2013 FireOneOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URLEncoding)

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;

@end
