//
//  Connection.h
//  ChordtabsInIpad
//
//  Created by Peerasak Unsakon on 1/11/13.
//  Copyright (c) 2013 FireOneOne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "AFNetworking.h"

@class Connection;

@protocol ConnectionDelegate <NSObject>

@optional

- (void)getImageUrlSucceesed:(NSURL *)chordUrl;

@end

@interface Connection : NSObject

+ (Connection *)getInstance;

- (void)googleSearchWithName:(NSString *)songName;
- (void)getImageURLFromLink:(NSString *)link;
- (void)urlRequestResponse:(NSString *)htmlString;


@property (weak, nonatomic) id <ConnectionDelegate> delegate;
@property (strong, nonatomic) NSDictionary *responseJSON;

@end
