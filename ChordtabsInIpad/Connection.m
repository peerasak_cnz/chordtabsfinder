//
//  Connection.m
//  ChordtabsInIpad
//
//  Created by Peerasak Unsakon on 1/11/13.
//  Copyright (c) 2013 FireOneOne. All rights reserved.
//

#import "Connection.h"

@interface Connection ()
{
    Connection *_connectionInstance;
}

@end

@implementation Connection

@synthesize delegate = _delegate;

static Connection *instance = nil;

+ (Connection *)getInstance
{
    @synchronized(self)
    {
        if (instance == nil) {
            instance = [Connection new];
        }
    }
    return instance;
}

- (void)googleSearchWithName:(NSString *)songName
{
    NSString *_chordPageUrl = [NSString stringWithFormat:@"http://www.google.co.th/webhp?#hl=en&q=%@+site:chordtabs.in.th&fp=1&bav=on.2,or.r_gc.r_pw.r_cp.r_qf.&cad=b", songName];
    
    NSError *_error;
    
    NSString *_htmlString = [NSString stringWithContentsOfURL:[NSURL URLWithString:_chordPageUrl] encoding:NSUTF8StringEncoding error:&_error];
    
    if (_error) {
        NSLog(@"Error = %@", _error);
    }else {
        NSLog(@"Ggoogle Search result: %@", _htmlString);
    }
        
}

- (void)getImageURLFromLink:(NSString *)link
{
    if ([link rangeOfString:@"song_id="].location != NSNotFound) {
        NSString *_songID = [link substringFromIndex:[link rangeOfString:@"song_id="].location + [link rangeOfString:@"song_id="].length];
        
        NSString *_chordPageUrl = [NSString stringWithFormat:@"http://chordtabs.in.th/song.php?posttype=webmaster&song_id=%@&chord=yes", _songID];
        
//        NSString *_htmlString = [NSString stringWithContentsOfURL:[NSURL URLWithString:_chordPageUrl] encoding:NSUTF8StringEncoding error:nil];
        
        NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:_chordPageUrl]];
        [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            NSString *_htmlString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            [self urlRequestResponse:_htmlString];
            
        }];
        
    }
}

- (void)urlRequestResponse:(NSString *)htmlString
{
    NSURL *_chordURL;
    NSString *_pattern;
    NSRegularExpression *_regex;
    
    _pattern = [NSString stringWithFormat:@"song/.*.png"];
    
    
    
    _regex = [NSRegularExpression
              regularExpressionWithPattern:_pattern
              options:NSRegularExpressionCaseInsensitive
              error:nil];
    
    NSArray *matches = [_regex matchesInString:htmlString
                                       options:0
                                         range:NSMakeRange(0, [htmlString length])];
    
    for (NSTextCheckingResult *match in matches) {
        NSRange range = [match rangeAtIndex:0];
        _chordURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://chordtabs.in.th/admin/admin/%@",[htmlString substringWithRange:range]]];
    }
    
    if ([matches count] > 0) {
        [_delegate getImageUrlSucceesed:_chordURL];
    }else{
        NSLog(@"Not Found");
    }
    
}


@end
